import { Component } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { QueueService } from '../services/queue.service';
import { catchError, of } from 'rxjs';

@Component({
  selector: 'app-viewer',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './viewer.component.html',
  styles: ``,
})
export class ViewerComponent {
  hn: any;
  departmentId: any;

  firstName = '';
  queueNo = '';
  departmentName = '';
  updatedAt = '';
  currentQueue = '';

  loading = false;
  timer: any;

  constructor(
    private route: ActivatedRoute,
    private queueService: QueueService,
    private router: Router
  ) {
    this.hn = this.route.snapshot.paramMap.get('hn');
    this.departmentId = this.route.snapshot.paramMap.get('departmentId');
  }

  ngOnInit() {
    this.getViewer();
    // Set timer
    this.timer = setInterval(() => {
      this.getViewer();
    }, 10000);
  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }

  getViewer() {
    this.loading = true;
    this.queueService
      .viewer(this.hn, this.departmentId)
      .pipe(
        catchError((error) => {
          console.error(error);
          return of(error);
        })
      )
      .subscribe((data) => {
        this.loading = false;

        if (!data.ok) {
          this.router.navigateByUrl('/verify');
          return;
        }

        this.firstName = data.info.first_name;
        this.queueNo = data.info.queue_no;
        this.departmentName = data.info.department_name;
        this.currentQueue = data.queue.queue_no;
        this.updatedAt = data.queue.updated_at;
      });
  }
}
