import { Component } from '@angular/core';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
import { QueueService } from '../services/queue.service';
import { catchError, of } from 'rxjs';

@Component({
  selector: 'app-verify',
  standalone: true,
  imports: [ReactiveFormsModule, RouterModule],
  templateUrl: './verify.component.html',
  styles: ``,
})
export class VerifyComponent {
  form = new FormGroup({
    hn: new FormControl('', [
      Validators.required,
      Validators.minLength(7),
      Validators.maxLength(7),
      Validators.pattern(/^[0-9]+$/),
    ]),
    departmentId: new FormControl(''),
  });

  isError = false;
  info: any;
  loading = false;

  departments: any = [];

  constructor(private router: Router, private queueService: QueueService) {}

  onSubmit() {
    if (this.form.valid) {
      this.loading = true;
      const hn: any = this.form.value.hn;
      this.queueService
        .verify(hn)
        .pipe(
          catchError((error) => {
            this.isError = true;
            console.error(error);
            return of(error);
          })
        )
        .subscribe((data) => {
          this.loading = false;

          if (data.ok) {
            const results = data.results;
            if (results.length === 0) {
              this.isError = true;
              return;
            }

            this.isError = false;
            this.departments = results;
            this.info = results[0];
          } else {
            this.isError = true;
          }
        });
    }
  }

  onReset() {
    this.isError = false;
    this.info = null;
    this.departments = [];
    this.form.reset();
  }

  gotoViewer() {
    const departmentId = this.form.value.departmentId;
    const hn = this.form.value.hn;

    this.router.navigate(['/viewer', hn, departmentId]);
  }
}
