import { Routes } from '@angular/router';
import { VerifyComponent } from './verify/verify.component';
import { ViewerComponent } from './viewer/viewer.component';

export const routes: Routes = [
  { path: '', redirectTo: 'verify', pathMatch: 'full' },
  {
    path: 'verify',
    component: VerifyComponent,
    title: 'ตรวจสอบข้อมูล',
  },
  {
    path: 'viewer/:hn/:departmentId',
    component: ViewerComponent,
    title: 'สถานะคิวบริการ',
  },
];
