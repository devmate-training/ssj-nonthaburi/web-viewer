import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class QueueService {
  constructor(private http: HttpClient) {}

  verify(hn: string) {
    const url = environment.apiUrl + '/verify';
    return this.http.post(url, { hn });
  }

  viewer(hn: string, departmentId: string) {
    const url = environment.apiUrl + '/viewer';
    return this.http.post(url, { hn, departmentId });
  }
}
